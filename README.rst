Dockerized Gnuradio
===================

This is a docker repository for quickly setting up `Gnuradio <https://www.gnuradio.org/>`_.

Getting Started
---------------

First you need Docker (and docker-compose). On Ubuntu, this is :code:`sudo apt install docker-compose`.

Then download this repository to a folder of choice and inside the folder, simply
run :code:`docker-compose up`. That's it!

You can save your works in the folder *workspace* in the docker container, which
is mapped to the local *workspace* folder. Saving your data anywhere else in the
container is not persistent and will be lost when closing the container.

Additional Notes
----------------

The docker-compose file is just a very convinient way to start the container.
If you prefer to run the Dockerfile via docker command, you can do so:

.. code-block:: bash

  docker build -t gnuradio .  # build the image
  xhost local:root  # add permission (only use if the next command fails)
  docker run -v /tmp/.X11-unix/:/tmp/.X11-unix -e DISPLAY gnuradio  # run image
