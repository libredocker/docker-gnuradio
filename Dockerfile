FROM ubuntu:16.04
RUN apt update && apt install -y gnuradio
RUN apt install -y rtl-sdr gr-osmosdr libusb-1.0-0-dev

WORKDIR /home/developer
RUN mkdir /home/developer/workspace
CMD gnuradio-companion
EXPOSE 5555

